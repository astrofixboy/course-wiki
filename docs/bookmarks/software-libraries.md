# Software Libraries


## Python - Web

- [Flask](https://flask.palletsprojects.com/en/2.1.x/#) is a micro web framework written in Python. 

- [Gunicorn](https://gunicorn.org/) 'Green Unicorn' is a Python WSGI HTTP Server for UNIX. It's a pre-fork worker model. The Gunicorn server is broadly compatible with various web frameworks, simply implemented, light on server resources, and fairly speedy.

- [gevent](http://www.gevent.org/) is a coroutine -based Python networking library that uses greenlet to provide a high-level synchronous API on top of the libev or libuv event loop.

- [Flask-SQLAlchemy](https://flask-sqlalchemy.palletsprojects.com/en/2.x/) is an extension for Flask that adds support for SQLAlchemy to your application. It aims to simplify using SQLAlchemy with Flask by providing useful defaults and extra helpers that make it easier to accomplish common tasks.

- [Flask-Security - flask-security-too](https://flask-security-too.readthedocs.io/en/stable/) allows you to quickly add common security mechanisms to your Flask application.

- [Flask-RESTful](https://flask-restful.readthedocs.io/en/latest/) is an extension for Flask that adds support for quickly building REST APIs. It is a lightweight abstraction that works with your existing ORM/libraries. Flask-RESTful encourages best practices with minimal setup. If you are familiar with Flask, Flask-RESTful should be easy to pick up.

- [flask-sse](https://flask-sse.readthedocs.io/en/latest/quickstart.html) A Flask extension for HTML5 server-sent events support, powered by Redis.


## Python - General

- [SQLAlchemy](https://www.sqlalchemy.org/) is the Python SQL toolkit and Object Relational Mapper that gives application developers the full power and flexibility of SQL.

- [Jinja](https://jinja.palletsprojects.com/en/3.1.x/) is a fast, expressive, extensible templating engine. Special placeholders in the template allow writing code similar to Python syntax. 

- [email-validator](https://pypi.org/project/email-validator/) Validate Email Addresses. A robust email address syntax and deliverability validation library for Python

- [bcrypt](https://pypi.org/project/bcrypt/) - Good password hashing for your software and your servers

- [redis-py](https://pypi.org/project/redis/) - The Python interface to the Redis key-value store.

- [Celery](https://docs.celeryq.dev/en/stable/) is a simple, flexible, and reliable distributed system to process vast amounts of messages, while providing operations with the tools required to maintain such a system. It’s a task queue with focus on real-time processing, while also supporting task scheduling.

- [WeasyPrint](https://pypi.org/project/weasyprint/) is a smart solution helping web developers to create PDF documents. It turns simple HTML pages into gorgeous statistical reports, invoices, tickets.



## Javascipt
- [JavaScript](https://developer.mozilla.org/en-US/docs/Learn/JavaScript) guide at Mozilla Developer Networks
- [Can I use](https://caniuse.com/) provides up-to-date browser support tables for support of front-end web technologies on desktop and mobile web browsers.

## Vue 2
- [Vue 2](https://v2.vuejs.org/) Documentation Website
- [Vue 2](https://v2.vuejs.org/v2/guide/#Getting-Started) Getting started
- [Vue Router 3 for Vue 2](https://v3.router.vuejs.org/guide/) Getting started guide
- [Vuex 3 for Vue 2](https://v3.vuex.vuejs.org/) getting started guide.
